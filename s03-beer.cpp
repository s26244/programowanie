#include <iostream>
#include <string>
using namespace std;

auto main() -> int
{
    int s;
    cout << "podaj liczbe: \n";
    cin >> s;
    
    for(int i = s; i > 0; i--)
    {
    cout << i << " bottles of beer on the wall, " << i << " bottles of beer. \n" << "Take one down, pass it around, " << i - 1 << " bottles of beer on the wall... \n";
    }
    
    cout << "No more bottles of beer on the wall, no more bottles of beer. \n" << "Go to the store and buy some more, " << s << " bottles of beer on the wall...";
}
