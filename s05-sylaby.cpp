#include <iostream>

auto main(int argc, char*argv[]) -> int
{
    int s;

    s = 0;

    auto const sentence = std::string{argv[1]};
    for(int i = 0; sentence[i]!='\0'; ++i)
    {
        if(sentence[i]=='a' || sentence[i]=='e' || sentence[i]=='i' ||
           sentence[i]=='o' || sentence[i]=='u' || sentence[i]=='A' ||
           sentence[i]=='E' || sentence[i]=='I' || sentence[i]=='O' ||
           sentence[i]=='U')
        {
            ++s;
        }
    }

    std::cout << s << std::endl;

    return 0;
}
