#include <iostream>
#include <random>
using namespace std;

auto main() -> int
{
    int n;
    cout << "podaj liczbe: \n";
    cin >> n;
    
    for(int i = 1; i <= n; i++)
    {
    	if(i % 3 == 0 && i % 5 == 0)
    	cout << i << " FizzBuzz" << "\n"; 
    
    	else if(i % 5 == 0)
    	cout << i << " Buzz" << "\n"; 
    
   	else if(i % 3 == 0)
    	cout << i << " Fizz" << "\n"; 
    	
    	else
    	cout << i << "\n";
    }
    
}
