#include <iostream>
#include <cstring>

auto main(int argc, charargv[]) -> int 
{
    auto const cenzura = std::string{argv[1]};
    auto const litery = std::string{argv[2]};

    char arr1[cenzura.length()];
    strcpy(arr1,cenzura.c_str());

    char arr2[litery.length()];
    strcpy(arr2,litery.c_str());

    int a = 0;

    if(cenzura.empty())
        {
            return -1;
        }
    if(litery.empty())
        {
            std::cout << cenzura;
            return 0;

        }
    for(int l = 0; l < cenzura.length(); l++)
        {
        if(arr1[l] == '')
            {
            arr1[l] = arr2[a];
            a++; 
            }
        std::cout << arr1[l];
        }
    return 0;
}
