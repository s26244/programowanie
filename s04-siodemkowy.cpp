#include <cstdlib>
#include <iostream>

 
 void dec_to_oct(int num)
{
    int l = 0, tab[31];
 
    while(num)
    {
        tab[l++] = num % 7;
        num /= 7;
    }
 
    for(int k = l - 1; k >= 0; k--)
        std::cout << tab[k];
}
 
 int main()
{
    int num;
 
    std::cout << "Podaj liczbe:";
    std::cin >> num;
 
    std::cout << "\nwynik:";
    dec_to_oct(num);
    std::cout << std::endl;
 
    system("pause");
    return 0;
 }
 
